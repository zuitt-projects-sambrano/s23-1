/*
	Activity:

	>> Using Robo3T, repopulate our users collections using CRUD operations (Same database: session23)

//Create
	>> Add at least five of the members of your favorite fictional group or musical band into our users collection with the following fields:

		-firstName -string
		-lastName -string
		-email -string
		-password -string
		-isAdmin -boolean 
			note: initially none of the users are admin.

	>> Add 3 new courses in a new courses collection with the following fields:

		-name - string
		-price - number
		-isActive - boolean 
			note: initially all courses should be inactive

	>> Note: If the collection you are trying to add a document to does not exist yet, mongodb will add it for you. 

//Read

	>> Find all regular/non-admin users.

//Update

	>> Update the first user in the users collection as an admin.
	>> Update one of the courses as active.

//Delete

	>> Delete all inactive courses


//Add all of your query/commands here in activity.js
*/

db.usersMany.insertMany([
    {
        "firstName" : "Reynaldo",
        "lastName" : "Rosali Jr.",
        "email" : "reynaldorosali@gmail.com",
        "password" : "user1234",
        "isAdmin" : "false"
    },
    {
        "firstName" : "Karim",
        "lastName" : "Lloren",
        "email" : "karimlloren@gmail.com",
        "password" : "user12345",
        "isAdmin" : "false"
    },
    {
        "firstName" : "Sheena Marie",
        "lastName" : "Suarez",
        "email" : "sheenamariesuarez@gmail.com",
        "password" : "user123456",
        "isAdmin" : "false"
    },
    {
        "firstName" : "Kim Aina",
        "lastName" : "de Ocampo",
        "email" : "kimainadeocampo@gmail.com",
        "password" : "user1234567",
        "isAdmin" : "false"
    },
    {
        "firstName" : "Sherylcion",
        "lastName" : "Sambrano",
        "email" : "sherylcion@gmail.com",
        "password" : "user12345678",
        "isAdmin" : "false"
    }
])

db.courses.insertMany([
    {
        "name" : "HTML",
        "price" : "1000",
        "isActive" : "false"
    },
    {
        "name" : "CSS",
        "price" : "1500",
        "isActive" : "false"
    },
    {
        "name" : "Javascript",
        "price" : "2000",
        "isActive" : "false"
    }
])

db.usersMany.find({})

db.usersMany.updateOne({}, {$set: {"isAdmin" : "true"}})

db.courses.updateOne({}, {$set: {"isActive" : "true"}})

db.courses.deleteMany({"isActive" : "false"})